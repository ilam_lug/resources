
IlamLUG Resources repo  

Files, presentations and other resources are dumped in this repository.  

Presentations  

this is where the files for each presentation gets saved.  

if you want to get a single file, you can use curl.  

for examlpe:  

        curl 'https://gitlab.com/ilamlug/resources/raw/master/presentations/session_x/session_x.pdf'  
    
logo  

svg, png format of ilamLUG logos in different colors.  

pixel  

modified version of the logos,circled for printing on pixels.  

<p dir="rtl">  

مخزن منابع لاگ ایلام  

فایل‌ها، ارايه‌ها و دیگر منابع در این مخزن قرار می گیرند.  

ارایه‌ها  

در این جا فایل‌های هر ارایه ذخیره می شوند.  

در صورتی که می‌خواهید یکی از فایل‌ها را داشته باشید میتوانید از دستور  curlاستفاده کنید.  

برای مثال : 
    
            curl 'https://gitlab.com/ilamlug/resources/raw/master/presentations/session_x/session_x.pdf  
    
لوگوهادر رنگ های مختلف با فرمت‌‌های svg،png در این مخزن قرار دارند.

</p>
